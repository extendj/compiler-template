public class MarkTheSpot {
  public static void main(String[] args) {
    // This is not a concatenation - will not be replaced.
    System.out.println("X marks the spot");
  }
}

/*EXPECT
X marks the spot
*/
